<!DOCTYPE html>
<html lang="en">
<head>
  <title>QEMU QED</title>
  <meta name="viewport" charset="utf-8" content="text/html, width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/css.css">
  <link rel="icon" href="img/EmuLogo.ico">
  <script src="js/jquery.min.js"></script>
  <script src="js/js.js"></script>
</head>

<body>
<div class="grid">
  <div class="header">
    <h1>QEMU Q.E.D.</h1>
  </div>

  <nav class="sidebar well">
    <h2 class="heading">Table of Contents</h2>
    <a href="#about"><div class="contents-list">About this resource</div></a>
    <a href="#installation"><div class="contents-list">Install QEMU</div></a>
    <a href="#architecture"><div class="contents-list">Calling/using QEMU</div></a>
    <a href="#hardware"><div class="contents-list">Assembling hardware</div></a>
    <a href="#networking"><div class="contents-list">Networking</div></a>
    <a href="#other-features"><div class="contents-list">Other important flags</div></a>
    <a href="#disk-images"><div class="contents-list">Creating and manipulating disk images</div></a>
    <a href="#software"><div class="contents-list">Necessary software</div></a>
    <a href="#console"><div class="contents-list">Using the QEMU console</div></a>
    <a href="#recipes"><div class="contents-list">Recipes for installing legacy operating systems</div></a>

  </nav>

  <div class="content">
    <div class="well">
      <h2 class="heading" id="about">About QEMU QED</h2>
      <p> QEMU (Quick EMUlator) is a powerful and flexible open-source command line application for computer emulation
        and virtualization. It is potentially of great use to archivists, conservators, and other digital preservation
        practitioners in need of recreating legacy computing systems, whether to interact with or migrate software-dependent data.
        But it is also a complex application with a vast range of configuration options. QEMU QED is intended to lower
        the barrier to first-time users by breaking down QEMU use into more digestible concepts and recipes.</p>
      <p>Dylan Lorenz's post/paper <a href="http://www.dylanlorenz.net/files/Floating-Time-with-QEMU-at-the-DAM.pdf">
        "<i>Floating Time</i> with QEMU at the Denver Art Museum"</a> provides an excellent, use-case-oriented overview
      of QEMU's advantages, complexities, and the need for a community-driven resource to guide new users through QEMU's
        thorough but often scattered existing documentation.</p>
      <p>QEMU QED is a work-in-progress. For full QEMU docs, check out the project's <a href="https://qemu.org/"
        target="_blank">official website</a>.</p>
      <!-- License info - repost when decided
      <span class="intro-lead">License</span>
      <p class="license">
        <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" src="img/cc.png"></a><br>
        This work is licensed under a <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License</a>.
      </p>
    -->
    </div>

    <div class="well">
    <h2 id="installation">Install QEMU</h2>
      <p>Recommendations for installing QEMU on different operating systems.</p>
      <!-- Install for BitCurator/Ubuntu -->
      <label class="recipe" for="ubuntu">BitCurator/Ubuntu</label>
      <input type="checkbox" id="ubuntu">
      <div class="hiding">
        <h3>BitCurator/Ubuntu</h3>
        <p>Check the <a href="https://www.qemu.org/download/#linux">official docs</a></p>
        <p class="link"></p>
      </div>
      <!-- End Install for BitCurator/Ubuntu -->

      <!-- Install for MacOS -->
      <label class="recipe" for="macos">MacOS</label>
      <input type="checkbox" id="macos">
      <div class="hiding">
        <h3>MacOS</h3>
        <p>QEMU is best installed on MacOS via <a href="https://brew.sh">Homebrew</a></p>
        <p><code>brew install qemu</code></p>
        <p class="link"></p>
      </div>
      <!-- Install for MacOS -->

      <!-- Install for Windows -->
      <label class="recipe" for="windows">Windows</label>
      <input type="checkbox" id="windows">
      <div class="hiding">
        <h3>Windows</h3>
        <p>Installation binaries can be found on the <a
        href="https://www.qemu.org/download/#windows">official documentation
        page</a>.</p>
        <p>TODO</p>
        <p class="link"></p>
      </div>
      <!-- Install for Windows -->

    </div>

    <div class="well">
      <h2 id="architecture">Calling/using QEMU</h2>
      <p>QEMU is technically a bundle of applications that emulate several different computing platforms or
        <a href="https://en.wikipedia.org/wiki/Instruction_set_architecture" target="_blank">architectures</a>.
        The first step in using QEMU is to decide which architecture is to be emulated, which will determine the command
       used to invoke QEMU from a command line terminal.</p>
      <p>This is not as complicated as it sounds. The best method is to figure out the target operating system you are trying to
        emulate (e.g. Windows 95) and work backwards. Generally, only a few platforms/commands are currently relevant for
        legacy material from the PC era.</p>
      <p><code>$ qemu-system-i386</code><p>
        <dd>Emulates the <a href="https://en.wikipedia.org/wiki/Intel_80386" target="_blank">Intel x86 32-bit architecture.</a>
          (aka "i386", "x86", "x86_32", or "IA-32")
          Relevant for IBM/PC/MS-DOS, all Windows systems up until 64-bit Windows XP, and many Linux, BSD, or other open
           source operating systems from, roughly, the late 1980s through the mid-2000s.</dd>
      <p><code>$ qemu-system-x86_64</code></p>
        <dd>Emulates <a href="https://en.wikipedia.org/wiki/X86-64" target="_blank">Intel x86 64-bit architecture.</a>
          (aka "x86_64", "x64")
          Can be used to run more recent PC operating systems like any Windows system newer than Windows XP 64-bit, or
          again many Linux, BSD, or other open source systems.</dd>
      <p><code>$ qemu-system-ppc</code></p>
        <dd>Emulates the <a href="https://en.wikipedia.org/wiki/PowerPC" target="_blank">PowerPC architecture.</a>
          Mostly relevant to emulate certain legacy MacOS versions (9.2.2, early OSX), some Linux systems.</dd>
      <p>All platforms currently supported by QEMU are documented <a href="https://wiki.qemu.org/Documentation/Platforms" target="_blank">
        here</a>, but again these will largely be used in edge cases for more specialized hardware.</p>
      <p>For the purpose of this guide, QEMU QED examples will use <code>qemu-system-i386</code>, except in specific
        <a href="#recipes">recipes</a> where required.</p>
      <br>
      <!-- Boot a CD-ROM -->
      <label class="recipe" for="boot-cd">Booting a downloaded CD-ROM file</label>
      <input type="checkbox" id="boot-cd">
      <div class="hiding">
        <h3>Boot a CD-ROM</h3>
        <p></p>
        <p><code>qemu-system-i386 -cdrom <i>filename.iso</i></code></p>
        <dl>
          <dt>qemu-system-i386</dt><dd>calls the program and specifies the i386 system architecture</dd>
          <dt>-cdrom</dt><dd>tells the program you are sending it a CD-ROM type of file</dd>
          <dt><i>filename.iso</i></dt><dd>path and name of the input file</dd>
        </dl>
        <p class="link"></p>
      </div>
      <!-- End Boot a CD-ROM -->
      <!-- Boot a hard drive -->
      <label class="recipe" for="boot-hdd">Booting a hard disk image</label>
      <input type="checkbox" id="boot-hdd">
      <div class="hiding">
        <h3>Boot a hard disk image</h3>
        <p></p>
        <p><code>qemu-system-i386 -drive,file=<i>filename.img</i></code></p>
        <dl>
          <dt>qemu-system-i386</dt><dd>calls the program and specifies the i386 system architecture</dd>
          <dt>-drive,file=</dt><dd>tells the program you are sending it a file mimicking a hard disk drive</dd>
          <dt><i>filename.img</i></dt><dd>path and name of the input file; QEMU is file extension-agnostic, so this
            could be <i>.dd</i>, <i>.qcow</i>, <i>.dmg</i>, etc.</dd>
        </dl>
        <p class="link"></p>
      </div>
      <!-- End Boot a hard drive -->
    </div>


    <div class="well">
    <h2 id="recipes">Recipes</h2>
    <p>The following are QEMU "recipes": example commands recommended and explained by QEMU QED contributors for
      installing the listed operating system. Hard disk drive and installation media disk images (floppies, CD-ROMs)
      must be created or sourced by the user - these recipes essentially serve as recommendations for assembling certain hardware
      and settings.</p>
    <p>Contributors should also note QEMU version used with their recipe, as this may affect command syntax or available
      options.</p>

    <!-- Windows 98 -->
    <label class="recipe" for="win98">Windows 98</label>
    <input type="checkbox" id="win98">
    <div class="hiding">
      <h3>Windows 98</h3>
      <p></p>
      <p><code>qemu-system-i386 -drive file=<i>Win98.img</i> -cdrom <i>Win98Install.iso</i> -m 512 -soundhw sb16 -vga cirrus
      -net nic,model=pcnet -boot d</code></p>
      <dl>
        <dt>qemu-system-i386</dt><dd>calls the program and specifies the i386 system architecture</dd>
        <dt>-drive file=<i>Win98.img</i></dt><dd>loads the specified file as a hard drive (ideally, a blank disk image)</dd>
        <dt>-cdrom <i>Win98Install.iso</i></dt><dd>loads the specified file (a bootable Windows 98 installation CD-ROM)
          into the CD-ROM drive</dd>
        <dt>-m 512</dt><dd>gives the emulated computer 512 MB of RAM</dd>
        <dt>-soundhw sb16</dt><dd>gives the emulated computer a SoundBlaster 16 sound card</dd>
        <dt>-vga cirrus</dt><dd>gives the emulated computer a Cirrus Logic video card</dd>
        <dt>-net nic,model=pcnet</dt><dd>gives the emulated computer an AMD PCNET ethernet adapter</dd>
        <dt>-boot d</dt><dd>directs the emulated computer to boot from the CD-ROM drive rather than the hard disk drive for
        installation; remove or switch to <code>-boot c</code> after successful install</dd>
      </dl>
      <p><b>tested with QEMU v2.11, v3.1</b></p>
      <p class="link"></p>
    </div>
    <!-- End Windows 98 -->

    <!-- Windows XP 32-bit -->
    <label class="recipe" for="winXP_32">Windows XP (32-bit)</label>
    <input type="checkbox" id="winXP_32">
    <div class="hiding">
      <h3>Windows XP (32-bit)</h3>
      <p></p>
      <p><code>qemu-system-i386 -drive file=<i>WinXP.img</i> -cdrom <i>WinXPInstall.iso</i> -m 512 -soundhw ac97 -vga cirrus
      -net nic,model=rtl8139 -enable-kvm -boot d</code></p>
      <dl>
        <dt>qemu-system-i386</dt><dd>calls the program and specifies the i386 system architecture</dd>
        <dt>-drive file=<i>WinXP.img</i></dt><dd>loads the specified file as a hard drive (ideally, a blank disk image)</dd>
        <dt>-cdrom <i>WinXPInstall.iso</i></dt><dd>loads the specified file (a bootable Windows XP installation CD-ROM)
          into the CD-ROM drive</dd>
        <dt>-m 512</dt><dd>gives the emulated computer 512 MB of RAM</dd>
        <dt>-soundhw ac97</dt><dd>gives the emulated computer an Intel AC'97 compatible sound card</dd>
        <dt>-vga cirrus</dt><dd>gives the emulated computer a Cirrus Logic video card</dd>
        <dt>-net nic,model=pcnet</dt><dd>gives the emulated computer a RealTek RTL8139 ethernet adapter</dd>
        <dt>-enable-kvm</dt><dd>allows QEMU to use KVM virtualization, emulated machine will run more efficiently (will likely require
          admin privileges on the host computer)
        <dt>-boot d</dt><dd>directs the emulated computer to boot from the CD-ROM drive rather than the hard disk drive for
        installation; remove or switch to <code>-boot c</code> after successful install</dd>
      </dl>
      <p><b>tested with QEMU v2.11, v3.1</b></p>
      <p class="link"></p>
    </div>
    <!-- End Windows XP 32-bit -->
    </div> <!-- end well -->

  </div><!-- ends "content" -->

<!-- sample example -->
<!--   <label class="recipe" for="*****unique name*****">*****Title****</label>
  <input type="checkbox" id="*****unique name*****">
  <div class="hiding">
Change the above data-target field, the hover-over description, the button text, and the below div ID
  <h3>*****Longer title*****</h3>
  <p><code>ffmpeg -i <em>input_file</em> *****code goes here***** <em>output_file</em></code></p>
  <p>This is all about info! This is all about info! This is all about info! This is all about info! This is all about info! This is all about info! This is all about info! This is all about info! This is all about info! This is all about info! This is all about info! This is all about info! This is all about info! This is all about info!</p>
  <dl>
    <dt>ffmpeg</dt><dd>starts the command</dd>
    <dt>-i <em>input file</em></dt><dd>path, name and extension of the input file</dd>
    <dt>*****parameter*****</dt><dd>*****comments*****</dd>
    <dt><em>output file</em></dt><dd>path, name and extension of the output file</dd>
  </dl>
</div> -->
<!-- ends sample example -->

<footer class="footer">
  <p>QEMU QED is built on code provided by <a href="https://amiaopensource.github.io/ffmprovisr" target="_blank">ffmprovisr</a>!
      Thank you to AMIA Open Source contributors.</p>
  <p>A project begun during the <a href="https://ipres2019.org" target="_blank">iPRES 2019</a> conference, as part of "Reading
    the Matrix: A Hackathon Linking Digital Forensics to User Access", sponsored by the teams behind <a href="https://softwarepreservationnetwork.org/eaasi" target="_blank">
    EaaSI</a> and <a href="https://bitcurator.net" target="_blank">BitCurator</a>.</p>
  <p>Contribute to QEMU QED through our <a href="https://gitlab.com/eaasi/qemu-qed" target="_blank">GitLab</a> page,
    or join the <a href="https://bit.ly/BC-EaaSI-Slack" target="_blank">Hack BitCurator and EaaSI</a> Slack workspace!</p>
</footer>
</div><!-- ends "grid" -->
</body>
</html>
